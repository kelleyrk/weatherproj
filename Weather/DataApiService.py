import requests
import json
from abc import ABC, abstractmethod

config_file = open('config.json')

config = json.load(config_file)

api_key = config['OpenWeather']['ApiKey']
weather_url = config['OpenWeather']['WeatherBaseUrl']
geo_url = config['OpenWeather']['GeoBaseUrl']
units = config['OpenWeather']['Units']


class GeoData(ABC):

    @abstractmethod
    def __init__(self):
        pass

    @abstractmethod
    def getLatLon(self, city, state=None):
        pass


class WeatherData(ABC):

    @abstractmethod
    def __init__(self, geoData: GeoData):
        pass

    @abstractmethod
    def getAllWeather(self, city, state=None) -> dict:
        pass



class OpenWeatherDataWeather(WeatherData):

    def __init__(self, geoData: GeoData=None):
        self.api_key = api_key
        self.geoData = geoData
        self.base_url = weather_url
        self.url = self.base_url+"?lat={}&lon={}&units={}&appid={}"

    def getAllWeather(self, city, state=None) -> dict:
        data = self.geoData.getLatLon(city=city, state=state)
        if data == None:
            return "There is no data for this location. Try a place that actually exists."
        lat, lon = data
        response = requests.get(self.url.format(lat, lon, units, self.api_key))
        return response.json()



class OpenWeatherDataGeoCode(GeoData):

        def __init__(self):
            self.api_key = api_key
            self.base_url = geo_url
            self.url = self.base_url+"?q={}&limit={}&appid={}"

        def getLatLon(self, city, state=None) -> tuple:
            response = requests.get(self.url.format(city, 5, self.api_key))
            data = response.json()

            if state != None:
                for data_point in data:
                    if 'state' in data_point and data_point['state'] == state:
                        return (data_point['lat'], data_point['lon'])

            return (data[0]['lat'], data[0]['lon'])


