# WeatherProj

Displays weather based on city and state.

## API Reference
#### SEE: https://openweathermap.org/api
- Get geographical coordinates at endpoint: geo/1.0/direct
- Get weather data at endpoint: data/2.5/weather

## Display

![WeatherProj](images/app_pic.png)
