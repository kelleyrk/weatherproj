import tkinter as tk
from DataApiService import OpenWeatherDataGeoCode, OpenWeatherDataWeather, WeatherData


class Window():
    def __init__(self, weather_data_source: WeatherData):
            self.weather_data_source = weather_data_source

            self.window = tk.Tk()
            self.window.title("Weather App")
            self.window.geometry("500x500")
            self.window.config(bg='#0f4b6e')

            self.city_entry = tk.Entry(self.window)
            self.city_label = tk.Label(self.window, text="City", bg='#0f4b6e', fg='white')
            self.state_entry = tk.Entry(self.window)
            self.state_label = tk.Label(self.window, text="State", bg='#0f4b6e', fg='white')

            self.city_label.pack()
            self.city_entry.pack(pady=10)
            self.state_label.pack()
            self.state_entry.pack(pady=10)

            self.submit = tk.Button(
                self.window,
                text="Get Weather",
                relief=tk.SOLID,
                command=self.displayWeather
            )
            self.submit.pack(pady=10)

            self.row_1 = tk.Label(self.window, bg='#0f4b6e', fg='white')
            self.row_2 = tk.Label(self.window, bg='#0f4b6e', fg='white')
            self.row_3 = tk.Label(self.window, bg='#0f4b6e', fg='white')
            self.row_4 = tk.Label(self.window, bg='#0f4b6e', fg='white')
            self.row_5 = tk.Label(self.window, bg='#0f4b6e', fg='white')
            # self.weather_data.place(relx=0.0, y=0.75, anchor='ne')

    def initialize(self):
        self.window.mainloop()


    def displayWeather(self):
        state_entry =  self.state_entry.get()
        city_entry =  self.city_entry.get()
        state = state_entry and state_entry or None
        city = city_entry and city_entry or None
        display_str = ""

        if city != None:
            current_weather = self.weather_data_source.getAllWeather(city=city, state=state)
            temp = current_weather['main']['temp']
            pressure = current_weather['main']['pressure']
            humidity = current_weather['main']['humidity']

            wind_speed = current_weather['wind']['speed']
            wind_heading = current_weather['wind']['deg']

            self.row_1.configure(text=f"Temp: {temp} °F")
            self.row_1.place(relx=0.1, rely=0.5)

            self.row_2.configure(text=f"Humidity: {humidity} %")
            self.row_2.place(relx=0.1, rely=0.55)

            self.row_3.configure(text=f"Pressure: {pressure} mbar")
            self.row_3.place(relx=0.1, rely=0.6)

            self.row_4.configure(text=f"Wind Speed: {wind_speed} kts")
            self.row_4.place(relx=0.1, rely=0.65)

            self.row_5.configure(text=f"Wind Heading: {wind_heading} °")
            self.row_5.place(relx=0.1, rely=0.7)

        else:
            self.row_1.configure(text="you must enter a valid city")
            self.row_1.place(relx=0.1, rely=0.5)

    



class App():
    def __init__(self, window: Window):
        self.window = window
    # def getWeather(self, city, state=None):
    #     return self.weather_data_source.getAllWeather(city=city, state=state)
    def run(self):
        self.window.initialize()



if __name__ == '__main__':
    weather_source = OpenWeatherDataWeather(OpenWeatherDataGeoCode())
    window = Window(weather_source)
    app = App(window)

    app.run()


